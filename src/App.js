import React, { useState, useEffect } from "react";
import styled from "styled-components";

import axios from "axios";

import Navbar from "./components/Navbar";
import CardMovie from "./components/CardMovie";

import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";

const Container = styled.div`
  display: flex;
  aling-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background-color: #d4ecdd;
`;

const BodyContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%;
  padding-top: 80px;
`;

export default function App() {
  const [movies, setMovies] = useState([]);
  const [movieName, setMovieName] = useState("a");
  const [timerCounter, setTimerCounter] = useState(false);

  const [amount, setAmount] = useState(() => {
    if (JSON.parse(localStorage.getItem("bracket")) !== null) {
      return JSON.parse(localStorage.getItem("bracket")).length;
    } else {
      return 0;
    }
  });

  const arrayStoreBracket = [];
  const [openDrawer, setOpenDrawer] = useState(false);

  const anchor = "right";

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setOpenDrawer(open);
  };

  const sumPrice = (items) => {
    var sum = 0;
    if (items !== null) {
      items.map((item, key) => {
        sum = sum + parseInt(item.price);
      });

      if (items.length > 3) {
        return sum - sum * 0.1;
      } else if (items.length > 5) {
        return sum - sum * 0.2;
      } else {
        return sum;
      }
    }
  };

  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=efd02a21dbddba12f1fa5aecb2e9f768&query=${movieName}`
      )
      .then((response) => {
        console.log(response.data.results);
        setMovies(response.data.results);
      });
  }, [movieName]);

  useEffect(() => {
    // console.log("bracket", bracket);
  }, [amount]);

  return (
    <Container>
      <Navbar
        setMovieName={setMovieName}
        amount={amount}
        setOpenDrawer={setOpenDrawer}
      />

      <BodyContainer>
        {movies.map((movie, index) => (
          <CardMovie
            key={index}
            items={movie}
            arrayStoreBracket={arrayStoreBracket}
          ></CardMovie>
        ))}
      </BodyContainer>
      <Drawer
        anchor={anchor}
        open={openDrawer}
        onClose={toggleDrawer(anchor, false)}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "500px",
            height: "100vh",
            backgroundColor: "#d4ecdd",
          }}
        >
          <h1 style={{ paddingLeft: "20px" }}>Bracket</h1>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            {amount > 0
              ? JSON.parse(localStorage.getItem("bracket")).map((item, key) => (
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",

                      width: "400px",
                      height: "80px",
                      margin: "5px",
                      border: "1px solid black",
                      borderRadius: "15px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        width: "300px",
                        alignItems: "center",
                      }}
                    >
                      <img
                        src={item.poster}
                        style={{
                          width: "auto",
                          height: "80px",
                          borderRadius: "15px 0px 0px 15px",
                        }}
                      />
                      {item.title}
                    </div>
                    <div style={{ width: "50px" }}>{item.price}</div>
                  </div>
                ))
              : null}
            {amount > 0 ? (
              <div>
                <h4>
                  Total price:{" "}
                  {sumPrice(JSON.parse(localStorage.getItem("bracket")))}{" "}
                </h4>
                <div style={{ display: "flex" }}>
                  <Button
                    variant="outlined"
                    onClick={() => {
                      localStorage.clear();
                    }}
                    style={{ marginRight: "20px" }}
                  >
                    Clear
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => {
                      setTimerCounter(true);
                    }}
                    style={{ marginLeft: "20px" }}
                  >
                    Check bill
                  </Button>
                </div>
              </div>
            ) : null}
          </div>
        </div>
      </Drawer>
    </Container>
  );
}
