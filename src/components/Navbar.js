import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { IconButton, Badge } from "@mui/material";

import SearchIcon from "@mui/icons-material/Search";
import LocalGroceryStoreIcon from "@mui/icons-material/LocalGroceryStore";
import ClearIcon from "@mui/icons-material/Clear";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 75px;
  background-color: #345b63;
  color: #fff;
  position: fixed;
  z-index: 99;
`;

const SearchBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 500px;
  height: 40px;
  background-color: #fff;
  border-radius: 20px;
`;

export default function Navbar({ setMovieName, amount, setOpenDrawer }) {
  const [clearInput, setClearInput] = useState(false);
  const [textSearch, setTextSearch] = useState("");

  const handleInput = (e) => {
    e.preventDefault();
    setMovieName(e.target.value);
    setTextSearch(e.target.value);

    setClearInput(true);
  };

  const handleClearInput = () => {
    setMovieName("a");
    setTextSearch("");

    setClearInput(false);
  };

  return (
    <Container>
      <h2 style={{ marginLeft: "30px" }}>Movies</h2>
      <SearchBar>
        <input
          style={{
            width: "100%",
            height: "30px",
            marginLeft: "30px",
            marginRight: "10px",
            border: 0,
            outline: "none",
          }}
          value={textSearch}
          onChange={handleInput}
        />

        {clearInput ? (
          <IconButton onClick={handleClearInput}>
            <ClearIcon />
          </IconButton>
        ) : null}

        <IconButton
          style={{
            marginRight: "30px",
          }}
        >
          <SearchIcon />
        </IconButton>
      </SearchBar>
      <IconButton
        color="primary"
        fontSize="large"
        style={{
          marginRight: "30px",
        }}
        onClick={() => {
          setOpenDrawer(true);
        }}
      >
        <Badge badgeContent={amount} color="secondary">
          <LocalGroceryStoreIcon />
        </Badge>
      </IconButton>
    </Container>
  );
}
