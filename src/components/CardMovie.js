import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Rating from "@mui/material/Rating";

const Container = styled.div`
  display: flex;
  flex-direction: column-reverse;
  width: 200px;
  height: 300px;
  background-color: #123456;
  border-radius: 15px;
  margin: 25px;
  cursor: pointer;
  :hover {
    opacity: 0.75;
  }
`;

const Body = styled.div`
  padding: 10px;
  color: #fff;
  background-color: #000;
  border-radius: 0 0 15px 15px;
  opacity: 0.75;
  min-height: 80px;
`;

export default function CardMovie({ items, arrayStoreBracket }) {
  let imgPath = "https://image.tmdb.org/t/p/w500/" + items.poster_path;

  const [open, setOpen] = useState(false);
  const [price, setPrice] = useState(0);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const addToBag = async () => {
    const myJson = {
      id: items.id,
      poster: imgPath,
      title: items.title,
      price: price,
    };

    arrayStoreBracket.push(myJson);

    await localStorage.setItem("bracket", JSON.stringify(arrayStoreBracket));

    await handleClose();

    // console.log(JSON.parse(localStorage.getItem("bracket")));
  };

  return (
    <Container
      style={{
        backgroundImage: `url(${imgPath})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
      }}
    >
      <Body onClick={handleClickOpen}>
        <h4>{items.title}</h4>
      </Body>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>{items.original_title}</DialogTitle>
        <DialogContent>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              padding: "10px",
            }}
          >
            <img
              src={imgPath}
              alt={items.title}
              style={{ width: "50%", height: "50%" }}
            />
          </div>
          <Rating
            name="simple-controlled"
            value={items.vote_average}
            max={10}
          />
          <DialogContentText>{items.overview}</DialogContentText>
        </DialogContent>
        <div
          style={{ display: "flex", justifyContent: "center", margin: "30px" }}
        >
          <TextField
            id="outlined-basic"
            label="Price"
            variant="outlined"
            style={{ width: "150px", marginRight: "10px" }}
            onChange={(e) => {
              setPrice(e.target.value);
            }}
          />

          <Button variant="contained" onClick={addToBag}>
            Add Bracket
          </Button>
        </div>
      </Dialog>
    </Container>
  );
}
